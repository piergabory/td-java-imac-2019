import java.util.Scanner; 

public class Calc { 
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int leftHandSideOperand = scanner.nextInt();
		int rightHandSideOperand = scanner.nextInt();
		
		System.out.println("Addition: " + (leftHandSideOperand + rightHandSideOperand));
		System.out.println("Substraction: " + (leftHandSideOperand - rightHandSideOperand));
		System.out.println("Product: " + (leftHandSideOperand * rightHandSideOperand));
		System.out.println("Euclidean Division: " + (leftHandSideOperand / rightHandSideOperand));
		System.out.println("Reminder: " + (leftHandSideOperand % rightHandSideOperand));
	}
}