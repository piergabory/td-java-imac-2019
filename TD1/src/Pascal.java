/**
 * La différence par le JIT
 * Triangle de pascal en C = 896ms
 * Triangle de pascal en Java = 324ms
 * (2,2 GHz Intel Core i7)
 */

public class Pascal { 
	private static int compute (int nBut, int pBut) {
		int[] tab = new int[nBut + 1];
		
		int n = 1, i = 0;
		tab[0] = 1;

		for (n = 1; n < tab.length; n++) {
			tab[n] = 1;
			for (i = n - 1; i > 0; i--) {
				tab[i] = tab[i-1] + tab[i];
			}
		}
		
		return tab[pBut];
	}
	
	public static void main(String[] arguments) {
		System.out.println(" Cn, p = " + Pascal.compute(30000, 250));
	}
}