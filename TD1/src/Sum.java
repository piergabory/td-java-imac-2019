public class Sum { 
	public static void main(String[] arguments) {
		int accumulator = 0;
		
		for (String argument : arguments) {
			try {
				accumulator += Integer.parseInt(argument);
			} catch (NumberFormatException e) {
				System.out.println("\"" + argument + "\" is not a number!");
			}
		}
		
		System.out.println(accumulator);
	}
}