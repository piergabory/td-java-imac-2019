class Test {
	public static void main(String[] args) {
		var first = args[0];
		var second = args[1];
		var last = args[2];
		
		// '' est évalué comme un char, "" comme une string. Les chars ont une meilleur performance.
		System.out.println(first + ' ' + second + ' ' + last);
	 }
}