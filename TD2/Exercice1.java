public final class Exercice1 {
	public static void main(String[] args) {

		var s = "toto";
		System.out.println(s.length());
		// 1. S est du type String. Le type est inféré à l'initialisation car on lui assigne une expression de type String.
		
		
		var s1 = "toto";
		var s2 = s1;
		var s3 = new String(s1);

		System.out.println(s1 == s2);
		System.out.println(s1 == s3);
		// 2. True s1 et s2 sont deux pointeurs sur la même allocation de mémoire
		//    False	s3 pointe sur une autre partie de la mémoire que s1 ou s2
		
		
		
		var s4 = "toto";
		var s5 = new String(s4);

		System.out.println(s4.compareTo(s5));
		// 3. compareTo va comparer la mémoire pointée par les deux variables, au lieu d'évaluer les adresses
		
		
		
		var s6 = "toto";
		var s7 = "toto";
		System.out.println(s6 == s7);
		// 4. 	Je suis pas sûr mais je pense que le compilateur a automatiquement optimisé la mémoire pour éviter de 
		// 		réallouer la même chose 2 fois... 
		//  	En effet, les string sont stockées en cache dans une piscine.
		
		// 5.	Les strings sont des objets cachés et partagés par le reste du code pour des raisons de performances. 
		// 		Elles sont constantes pour éviter que changer une string affecte du code ayant utilisé la même expression ailleur.
		// 		https://stackoverflow.com/a/22398067
		
		
		var s8 = "hello";
		s8.toUpperCase();
		System.out.println(s8);
		// 6 toUpperCase __Retourne__ une copie de l'instance de string en capitales. Il n'y a pas de mutation de s8.
	}
}