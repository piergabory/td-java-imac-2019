import java.util.regex.*;

class IPParser {
	public static int[] parse(String source) {
												// check for three number between 255 and 0 followed by a dot then followed by another number. Accepts leading zeros
		Pattern ippattern = Pattern.compile("\\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b");
		Matcher matcher = ippattern.matcher(source);
		
		int[] result = {0, 0, 0, 0};
		
		if (matcher.find()) {
			Pattern dotpattern = Pattern.compile("\\.");
			String extract = matcher.group();
			String[] splittedStringAddress = dotpattern.split(extract);
			var index = 0;

			for (String stringByte : splittedStringAddress ) {
				result[index] = Integer.parseInt(stringByte);
				index++;
			}
		}

		return result;
	}
}

class Regex {
	public static void main(String[] args) {
		// 1. 	Pattern contien une regex, Compile permet de l'interpréter 
		// 		Matcher permet d'évaluer une regex interprétée sur une String		
		
		Pattern onlynumbers = Pattern.compile("\\d+");
	
		System.out.println("nombre");
		for (String string : args) {
			if (onlynumbers.matcher(string).matches()) {
				System.out.println(string);
			}
		}

		System.out.println("tout nombre");
		for (String string : args) {
			var matcher = onlynumbers.matcher(string);
			if (matcher.find()) System.out.print(matcher.group());
		}
		
		System.out.println();
		System.out.println("---");

		var ip = IPParser.parse("source 212.323.132.21 33.013.054.244");
		// leading zeros in the adress will be truncated
		for (int b : ip) {
			System.out.print(b + ".");
		}
	}
}