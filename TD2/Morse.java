class Morse {
	private static String stop = " Stop. ";
	
	// 1.
	public static void print(String[] message) {
		for (String word : message) {
			System.out.print(word + stop);
		}
	}
	
	
	// 3.
	public static String build(String [] message) {
		var morseMessage = new StringBuilder("");
		for (String word : message) {
			morseMessage.append(word);
			morseMessage.append(stop);
		}
		return morseMessage.toString();
	}
}


class Untitled {
	public static void main(String[] args) {
		Morse.print(args);
		
		// 2. Stringbuilder est une string mutable.
		
		System.out.println();
		System.out.println(Morse.build(args));
		
		
		// 4. '' est évalué comme un char, "" comme une string. Les chars ont une meilleur performance.
		
		// 5 "foo" + "bar" -> "foobar",  "foo".append("bar") -> objet ordonnant "foo" et "bar".
		// avec append, on évite d'allouer des strings temporaire pour rien. C'est particulièrement intéressant pour des construction dynamiques de string avec des boucles for. On réservera alors + pour des string statiques, car plus simple.
	}
}