import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println("\n\n# Exercice 1 -------------------");

        var book = new Book("LOTR", "Tolkien");


        // book.title = "LOTR";
        // book.author = "Tolkien"; 

        // does not work if the fields private or protected.
        // i can only access public properties.
        // static properties do not belong to the book itself but to the book type

        // we need a constructor and 'get' accessors because public methods are bad practice 
        // for breaking the encapsulation principles.

        System.out.println(book.getTitle() + " by " + book.getAuthor());

        // we add final to the private members of Book in order to the mutablity of the members.
        // this make sure they're not mutated in sub-classes made by other developers.

        // le bon constructeur est utilisé selon les arguments passés, car deux constructeurs ne peuvent avoir les même paramètres.
        // this() appelle le premier constructeur 

        System.out.println("\n\n# Exercice 2 -------------------");

        var b1 = new Book("Da Java Code", "Duke Brown");
        var b2 = b1;
        var b3 = new Book("Da Java Code", "Duke Brown");

        System.out.println(b1 == b2); // true
        System.out.println(b1 == b3); // false

        // == compare les valeurs de pointeurs.
        // b1 et b2 sont deux pointeurs sur le meme objet du tas
        // b3 est un nouveau pointeur sur une autre zone du tas

        System.out.println(b1.equals(b3)); // false

        // "Returns the index of the first occurrence of the specified element in this list"
        var list = new ArrayList<Book>();
        list.add(b1);
        System.out.println(list.indexOf(b2));
        System.out.println(list.indexOf(b3));

        // les index sont à 0. B3 n'as pas été ajouté car il 'existe déjà' étant donné qu'il est égal à b2.
        // on en déduit que ArrayList.add() utilise .equals pour éviter les duplications
        // si Book.equals() n'était pas déjà un override de Object.equals(), ArrayList.add() aurait inséré à l'index -1

        /*
        var aBook = new Book(null, null);
        var anotherBook = new Book(null, null);
        var list = new ArrayList<Book>();
        list.add(aBook);
        System.out.println(list.indexOf(anotherBook));
        */

        // l'erreur est à ArrayList.add, ou la méthode appelle equals sur un livre à champs nulls.
        // l'interpréteur crash quand il essaie de comparer une string à null.

        // il est préférable d'interdire une initialisation à null que de perdre des ressources à vérifier à 
        // chaque lecture de la variable. On utilisera Objects.requireNonNull(variableAChecker, MessageAuDeveloppeur);

        System.out.println("\n\n# Exercice 3 -------------------");

        var printablebook = new Book("Da Java Code", "Dan Duke");
        System.out.println(printablebook);

        // overridons Object.toString()

        var anonymousBookA = new Book("Dream of the Red Chamber");
        System.out.println(anonymousBookA);

        var anonymousBookB = new Book("Robin Hood", "anonymous");
        System.out.println(anonymousBookB);
    }
}