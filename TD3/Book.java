import java.util.Objects;

// Answers to questions are in Main.java

public class Book { 

    // properties
    final private String title;
    final private String author;
    final private boolean isAuthorKnown;

    static final private String fallbackTitle = "untitledBook";
    static final private String fallbackAuthor = "anonymous";

    // accessors
    public String getTitle() { return title; }
    public String getAuthor() { return author; }

    // constructor
    private Book(String title, String author, boolean known) {
        Objects.requireNonNull(title, "Null titles are not allowed");
        Objects.requireNonNull(author, "Null authors are not allowed");

        this.title = title;
        this.author = author;
        this.isAuthorKnown = known;
    }
    
    public Book(String title, String author) {
        this(title, author, true);
    }

    public Book(String title) {
        this(title, fallbackAuthor, false);
    }

    // default constructor
    public Book() {
        this(fallbackTitle);
    }

    // compare method (Object)
    @Override
    public boolean equals(Object target) {
        if (target instanceof Book) {
            var targetBook = (Book)target;
            return title.equals(targetBook.title) && author.equals(targetBook.author);
        }

        return false;
    }

    @Override
    public String toString() {
        return title + (isAuthorKnown ? (" by " + author) : "") + ".";
    }

	public static void main(String[] args) {
        var book = new Book();

        System.out.println(book.title + " by " + book.author);
    }
}