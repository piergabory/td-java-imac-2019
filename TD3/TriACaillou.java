public class TriACaillou {
    public static void swap(int[] array, int leftIndex, int rightIndex) {
        int handle = array[leftIndex];
        array[leftIndex] =  array[rightIndex];
        array[rightIndex] = handle;
    }

    public static int boundedIndexOfMin(int[] array, int begin, int stop) {
        int min =  array[begin];
        int minIndex = begin;
        for (int index = begin; index < stop && index < array.length; index++) if (min > array[index]) {
            min = array[index];
            minIndex = index;
        }
        return minIndex;
    }

    public static int indexOfMin(int[] array) {
        return boundedIndexOfMin(array, 0, array.length);
    }

    public static void sort(int[] array) {
        for (int index = 0; index < array.length; index++) {
            swap(array, index, boundedIndexOfMin(array, index, array.length));
        }
    }

    public static void main(String[] args) {
        int[] test = {8, 1, 3, 2, 4, 3};
        sort(test);
        for (int value : test) System.out.print(value + " ");
    }
}